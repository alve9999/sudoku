/**
 * 
 */
package sudoku;


public class Sudoku implements SudokuSolver {

	private int[][] grid;

	public Sudoku() {
		grid = new int[9][9];
	}

	@Override
	public boolean solve() {
		if(!isAllValid()) {
			return false;
		}
		return solve(0,0);
	}
	
	private boolean solve(int row ,int col) {
		if(row==9){
			row=0;
			col++;
			if(col==9) {
				return true;
			}
		}
		if(grid[row][col]!=0) {
			if(isValid(row,col)) {
				return solve(row+1,col);
			}
			else {
				return false;
			}
		}
		for(int i = 0; i < 9; i++) {
			Boolean solved = false;
			set(row,col,i+1);
			if(isValid(row,col)) {
				solved = solve(row+1,col);
			}
			if(solved) {
				return true;
			}
			clear(row,col);
		}
		return false;
		
	}
	
	@Override
	public void set(int row, int col, int digit) {
		if(row>8 || col>8 || row<0 || col<0 || digit>9 || digit<0) {
			throw new IllegalArgumentException("out of bounds");
		}
		grid[row][col]=digit;
		

	}

	@Override
	public int get(int row, int col) {
		if(row>8 || col>8 || row<0 || col<0) {
			throw new IllegalArgumentException("out of bounds");
		}
		return grid[row][col];
	}

	@Override
	public void clear(int row, int col) {
		if(row>8 || col>8 || row<0 || col<0) {
			throw new IllegalArgumentException("out of bounds");
		}
		grid[row][col]=0;

	}

	@Override
	public void clearAll() {
		for(int i = 0; i < 9; i++) {
			for(int j = 0; j < 9; j++) {
				clear(i,j);
			}
		}

	}

	@Override
	public boolean isValid(int row, int col) {
		if(row>8 || col>8 || row<0 || col<0) {
			throw new IllegalArgumentException("out of bounds");
		}
		if(grid[row][col]==0) {
			return true;
		}
		for (int i = 0; i < 9; i++) {
			if(grid[row][col]==grid[row][i] && i!=col) {
				return false;
			}
		}
		for (int i = 0; i < 9; i++) {
			if(grid[i][col]==grid[row][col] && i!=row) {
				return false;
			}
		}    
		int boxRowStart = row - row % 3;
	    int boxColStart = col - col % 3;
	    for (int i = boxRowStart; i < boxRowStart + 3; i++) {
	        for (int j = boxColStart; j < boxColStart + 3; j++) {
	            if (grid[i][j] == grid[row][col] && (i != row || j != col)) {
	                return false;
	            }
	        }
	    }
        return true;
	}

	@Override
	public boolean isAllValid() {
		for(int i = 0; i < 9; i++) {
			for(int j = 0; j < 9; j++) {
				if(!isValid(i,j)) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void setGrid(int[][] m) {
        if (m.length != 9 || m[0].length != 9) {
            throw new IllegalArgumentException("Invalid grid dimensions");
        }

		for(int i = 0; i < 9; i++) {
			for(int j = 0; j < 9; j++) {
				set(i,j,m[i][j]);
			}
		}
	}

	@Override
	public int[][] getGrid() {
        int[][] copy = new int[9][9];
        for (int i = 0; i < 9; i++) {
        	for (int j = 0; j < 9; j++) {
        		copy[i][j]=grid[i][j];
        	}
        }
        return copy;
	}

}
