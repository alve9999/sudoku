package sudoku;
import javax.swing.*;
import java.awt.*;



public class SudokuGUI {
	private SudokuSolver solver;
    private JTextField[][] textFields;
    private JButton solveButton;
    private JButton clearButton;
    
    private boolean isValidInput(String input) {
    	if(input.isEmpty()) {
    		return true;
    	}
        try {
            int value = Integer.parseInt(input);
            return value >= 1 && value <= 9;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    
    private boolean areAllInputsValid() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                String value = textFields[i][j].getText();
                if (!isValidInput(value)) {
                    return false;
                }
            }
        }
        return true;
    }
    
	public SudokuGUI() {
        solver = new Sudoku();
        textFields = new JTextField[9][9];
        
        JFrame frame = new JFrame("Sudoku");
        
    	JPanel mainPanel = new JPanel(new GridLayout(9, 9));
    	
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                textFields[i][j] = new JTextField(1);
                Font font = new Font("Arial", Font.PLAIN, 80 * 2 / 3);
                textFields[i][j].setFont(font);
                mainPanel.add(textFields[i][j]);
                int boxRow = i / 3;
                int boxCol = j / 3;
                if ((boxRow + boxCol) % 2 == 0) {
                    textFields[i][j].setBackground(Color.LIGHT_GRAY);
                }
            }
        }

        solveButton = new JButton("Solve");
        clearButton = new JButton("Clear");
        
        
        
        clearButton.addActionListener((e) -> {
        	for(int i = 0; i<9;i++) {
        		for(int j = 0; j<9;j++) {
        			textFields[i][j].setText("");
        		}
        	}
        });
        solveButton.addActionListener((e) -> {
        	solver.clearAll();

        	if(!areAllInputsValid()) {
        		JOptionPane.showMessageDialog(frame,"Invalid input. Please enter integers between 1 and 9.");
        		return;
        	}
        	for(int i = 0; i<9;i++) {
        		for(int j = 0; j<9;j++) {
        			if(!textFields[i][j].getText().isEmpty()) {
        				solver.set(i, j,Integer.parseInt(textFields[i][j].getText()));
        			}
        		}
        	}
        	if(!solver.solve()) {
        		JOptionPane.showMessageDialog(frame,"sudoku cant be solved");
        	}
        	else {
            	for(int i = 0; i<9;i++) {
            		for(int j = 0; j<9;j++) {
            			textFields[i][j].setText(Integer.toString(solver.get(i,j)));
            		}
            	}
        	}
        });
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(solveButton);
        buttonPanel.add(clearButton);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(mainPanel,BorderLayout.CENTER);
        frame.add(buttonPanel,BorderLayout.SOUTH);
        frame.pack();
        frame.setVisible(true);
        
	}

	public static void main(String[] args) {
		new SudokuGUI();
	}



}
