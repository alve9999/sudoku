package sudoku;

import org.junit.jupiter.api.Assertions.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SudokuTest extends Sudoku {
	private Sudoku sudokuSolver;
	@BeforeEach
	void setUp() throws Exception {
		sudokuSolver = new Sudoku();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSetAndGet() {
		sudokuSolver.set(0, 0, 5);
		assertEquals(5,sudokuSolver.get(0, 0));
		assertThrows(IllegalArgumentException.class,()->sudokuSolver.get(10, 10));
		assertThrows(IllegalArgumentException.class,()->sudokuSolver.set(1, 1,10));
	}
	@Test
	void testClear() {
		sudokuSolver.set(1, 1, 5);
		sudokuSolver.clear(1, 1);
		assertEquals(0,sudokuSolver.get(1, 1));
		assertThrows(IllegalArgumentException.class,()->sudokuSolver.clear(10, 10));
	}
    @Test
    void testIsValid() {
        sudokuSolver.set(2, 2, 9);
        assertTrue(sudokuSolver.isValid(2, 2));
        assertThrows(IllegalArgumentException.class,()->sudokuSolver.isValid(10, 10));

    }
    @Test
    void testIsNotValid() {
        sudokuSolver.set(0, 0, 4);
        sudokuSolver.set(0, 1, 4);
        assertFalse(sudokuSolver.isValid(0, 0));
        sudokuSolver.clear(0, 1);
        sudokuSolver.set(1, 0, 4);
        assertFalse(sudokuSolver.isValid(0, 0));
        sudokuSolver.set(1, 1, 4);
        sudokuSolver.clear(1, 0);
        assertFalse(sudokuSolver.isValid(0, 0));
        assertFalse(sudokuSolver.isAllValid());
        
    }
    @Test
    void testSetGrid() {
        int[][] grid = {
            {1, 2, 3, 0, 0, 0, 0, 0, 0},
            {4, 5, 6, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 7, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0}
        };
        sudokuSolver.setGrid(grid);
        assertArrayEquals(grid, sudokuSolver.getGrid());
        grid[8][8]=20;
        assertThrows(IllegalArgumentException.class,()->sudokuSolver.setGrid(grid));
        int[][] grid2 = {
                {1, 2, 3, 0, 0, 0, 0, 0, 0,0}
            };
        assertThrows(IllegalArgumentException.class,()->sudokuSolver.setGrid(grid2));
    }
    @Test
    void testClearAll() {
		sudokuSolver.set(1, 1, 5);
		sudokuSolver.set(1, 2, 5);
		sudokuSolver.clearAll();
		assertEquals(0,sudokuSolver.get(1, 1));
		assertEquals(0,sudokuSolver.get(1, 2));
    }
    @Test
    void testSolve() {
    	assertTrue(sudokuSolver.solve());
    	
        int[][] grid = {
                {1, 2, 3, 0, 0, 0, 0, 0, 0},
                {4, 5, 6, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 7, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}
            };
        sudokuSolver.setGrid(grid);
        assertFalse(sudokuSolver.solve());
        int[][] grid2 = {
                {0, 0, 8, 0, 0, 9, 0, 6, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {1, 0, 2, 5, 0, 0, 0, 0, 0},
                {0, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 5, 0, 0, 0, 0, 6, 0, 0},
                {6, 0, 0, 0, 0, 0, 0, 2, 8},
                {4, 1, 0, 6, 0, 8, 0, 0, 0},
                {8, 6, 0, 0, 3, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 4, 0, 0}
            };
        sudokuSolver.setGrid(grid2);
        assertTrue(sudokuSolver.solve());
    }
    
}
